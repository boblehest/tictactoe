# Networked Tic-Tac-Toe

This is a toy project that I created to familiarize myself with Rust and its
ecosystem.

## How to run

1. [Install the Rust compiler](https://www.rust-lang.org/tools/install).
2. Clone this repository: `git clone https://bitbucket.org/boblehest/tictactoe`.
3. Enter the project folder: `cd tictactoe`.
4. Update submodules: `git submodule update --init`.
5. Start the client: `cargo run --bin tictactoe_client_relm`.
6. (Optional) Start the server for multiplayer features: `cargo run --bin server`.

## Pictures

Online lobby list

![Lobby list](img/lobby_list.png)

Game board

![Game board](img/offline_game.png)
